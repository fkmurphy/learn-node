
const express = require('express');
const app = express();

const { config } = require('./config/index');
const {
    logErrors,
    wrapErrors,
    errorHandler 
} = require('./utils/middleware/errorHandlers.js');

const notFoundHandler = require('./utils/middleware/notFoundHandler');

//data json parse (middleware body parser)
app.use(express.json());

app.get('/', function(req,res) {
    res.send("Hello world");
})

app.get('/json', function (req, res) {
    res.json({hello: 'world'});
});


const productsApi = require('./routes/products');

productsApi(app);
//after productsApi, if route not exists.. error 404
app.use(notFoundHandler);

//middlewares de error al final de las rutas (estas utlimas tmb son middlewares)
//error handlers
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);

app.listen(config.port, function() {
    console.log("Listen in port " + config.port);
})

