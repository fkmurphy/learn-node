const { config } = require('../../config');
const boom = require('@hapi/boom');

function withErrorStack(error, stack) {
    if (config.dev) {
        return {error, stack};
    }
    return error;
}

//middleware
function logErrors(err, req, res, next) {
    console.log(err);
    next(err);
}

function wrapErrors(err, req, res, next) {
    if (!err.isBoom) {
        next(boom.badImplemetation(err));
    }
    next(err);
}

//middleware 4 params
function errorHandler(err, req, res, next) { // eslint-disable-line (no check unused param)
    //res.status(err.status || 500);
    const {
        output: {statusCode, payload}
    } = err;

    res.status(statusCode);
    //res.json(withErrorStack(err.message, err.stack));
    res.json(withErrorStack(payload, err.stack));
}

module.exports = {
    logErrors,
    wrapErrors,
    errorHandler
};
