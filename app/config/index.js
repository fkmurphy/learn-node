require('dotenv').config();

const config = {
    dev: process.env.NODE_ENV !== 'production',
    port: process.env.PORT || 8080,
	cors: process.env.CORS || '',
	mongoDb: process.env.MONGO_DB || '',
	mongoUser: process.env.MONGO_USER || '',
	mongoPass: process.env.MONGO_PASS || '',
	mongoHost: process.env.MONGO_HOST || '',
	mongoPort: process.env.MONGO_PORT || '27017'
}

module.exports = {config}
