//const { productsMock } = require('../utils/mocks/products');
const MongoLib = require('../lib/mongo');

class ProductsService {
    constructor() {
        this.collection = 'products';
        this.mongoDB = new MongoLib();
    }
    async getProducts({ tags }) {
        //const products = await Promise.resolve(productsMock);
        const query = tags && {
            tags: { $in: tags }
        };
        const products = await this.mongoDB.getAll(this.collection, query);
        return products || [];
    }

    async getProduct({ productId }) {
        console.log("EL PRODUCT ID ::: : : : ",productId)
        //const product = await Promise.resolve(productsMock[0]);
        const product = await this.mongoDB.get(this.collection, productId)
        return product || {};
    }

    async createProduct({ product }) {
        //const product = await Promise.resolve(productsMock[0].id);
        const productCreate = await this.mongoDB.create(this.collection, product);
        return productCreate;
    }

    async updatedProduct({ productId, product } = {}) {
        //const product = await Promise.resolve(productsMock[0].id);
        const productUpdate = await this.mongoDB.update(this.collection, productId, product);
        return productUpdate;
    }

   async deleteProduct({ productId }) {
        //const product = await Promise.resolve(productsMock[0].id);
        const product = await this.mongoDB.delete(this.collection, productId);
        return product;
    }
}

module.exports = ProductsService
