FROM node:latest AS test

WORKDIR /usr/src/app

COPY ./app/package*.json ./

RUN npm install
# RUN npm ci --only=production

COPY . .

EXPOSE 8080

#CMD [ "npm", "test" ]

FROM node:latest

WORKDIR /usr/src/app
COPY ./app/package*.json ./
RUN npm install
COPY . .
CMD [ "npm", "run", "dev" ]
