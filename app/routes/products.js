const express = require('express');
const ProductsService = require('../services/products');

const {
    productIdSchema,
    updateProductSchema,
    createProductSchema
} = require('../utils/schemas/products');

const validationHandler = require('../utils/middleware/validationHandler');

function productsApi(app) {
    const router = express.Router();
    app.use('/api/products', router);
    const productsServices = new ProductsService();

    router.get("/", async function(req, res, next){

        const {tags} = req.query;
        try{
            //const products = await Promise.resolve(productsMock);
            const products = await productsServices.getProducts({tags});
            //throw new Error('Error products'); check use error middleware
            res.status(200).json({
                data: products,
                message: 'prods listed'
            });

        } catch(err) {
            next(err)
        }
    })

    //router.get("/:productId", validationHandler({productId: productIdSchema}, 'params'),async function(req, res, next){
    router.get("/:productId", async function(req, res, next){
        const {productId} = req.params;
        try{
            //const product = await productsServices.getProduct({productId});
            const product = await productsServices.getProduct(productId);
            res.status(200).json({
                data: product,
                message: 'prod get'
            });

        } catch(err) {
            next(err)
        }
    });

    router.post("/", validationHandler(createProductSchema), async function(req, res, next){
        const {productId} = req.params;
        const {body: product} = req;
        try {
            const createdProductId = await productsServices.createProduct({product});
            res.status(201).json({
                data: createdProductId,
                message: 'se crea un prod'
            })
        } catch (err) {
            next(err)
        }

    });


    router.put("/:productId", validationHandler({productId: productIdSchema}, 'params'), validationHandler(updateProductSchema), async function(req, res, next){
        const {productId} = req.params;
        const {body: product} = req;
        try {
            const updatedProductId = await productsServices.updatedProduct({productId, product});
            res.status(200).json({
                data: updatedProductId,
                message: 'se actualiza un prod'
            })
        } catch (err) {
            next(err)
        }

    });


    router.delete("/:productId", async function(req, res, next){
        const {productId} = req.params;
        try {
            const deleteProductId = await productsServices.deleteProduct({productId});
            res.status(200).json({
                data: deleteProductId,
                message: 'se elimina un prod'
            })
        } catch (err) {
            next(err)
        }

    });
    //router.get("/:id", async function(req, res, next){});
}

module.exports = productsApi;
