const assert = require('assert');
const proxyquire = require('proxyquire');

const { productsMock, ProductsServiceMock } = require('../utils/mocks/products.js');
const testServer = require('../utils/testServer');

describe('routes - products', function() {

    //replace for this route by ProductsServiceMock
    // this is a stub, provide data o responses
    const route = proxyquire('../routes/products', {
        '../services/products': ProductsServiceMock
    });

    const request = testServer(route);

    describe('GET /products', function() {
        //this should response with status = 200
        it('should response status 200', function(done) {
            request.get('/api/products').expect(200, done);
        });

        it('should response with the list of products', function(done) {
            request.get('/api/products').end((err, res) => {

                assert.deepEqual(res.body, {
                    data: productsMock,
                    message: 'prods listed'
                });
                //finish test use done()
                done();
            });
        });

        it('should respons with a product', (done) => {
            const productId = 2;
            request.get(`/api/products/${productId}`).end((err,res) => {
                assert.deepEqual(res.body, {
                    data: productsMock[1],
                    message: 'prod get'
                });
            });
            done();
        });
    });


});

