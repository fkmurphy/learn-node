const { MongoClient, ObjectId } = require('mongodb');
const { config } = require('../config/index');

//special character
const USER = encodeURIComponent(config.mongoUser);
const PASSWORD = encodeURIComponent(config.mongoPass);
const DB_NAME = encodeURIComponent(config.mongoDb);
//const MONGO_URI = `mongodb+srv://${USER}:${PASSWORD}@${config.mongoHost}:${config.mongoPort}/${DB_NAME}?retryWrites=true&w=majority`;
const MONGO_URI = `mongodb://${USER}:${PASSWORD}@${config.mongoHost}:${config.mongoPort}/${DB_NAME}?retryWrites=true&w=majority`;

class MongoLib {
    constructor() {
        //this.client = new MongoClient(MONGO_URI, { userNewUrlParser: true });
        console.log(MONGO_URI);
        this.client = new MongoClient(MONGO_URI);
        this.dbName = DB_NAME;
    }

    /**
     * Singleton
     */
    connect() {
        if (!MongoLib.connection) {
            MongoLib.connection = new Promise((resolve, reject) => {
                this.client.connect( (err, db) => {
                    if (err) {
                        reject(err)
                    }
                    console.log('Conexion agregado');
                    resolve(this.client.db(this.dbName));
                });
            });//.then(() => {this.client.db(DB_NAME)});
        }
        return MongoLib.connection;
    }


    getAll(collection, query) {
        return this.connect()
            .then(db => {
                return db.collection(collection).find(query).toArray();
            })
    }

    get(collection, id) {
        return this.connect()
            .then(db => {
                return db.collection(collection).findOne({_id: ObjectId(id)});
            })
    }

    create(collection, data) {
        console.log(data);
        return this.connect()
            .then(db => {
                return db.collection(collection).insertOne(data);
            })
            .then(result => result.insertedId)
    }

    update(collection, id, data) {
        return this.connect()
            .then(db => {
                return db.collection(collection).updateOne(
                    {
                        _id: ObjectId(id)
                    },
                    {
                        $set: data
                    },
                    {upsert: true}
                );
            })
            .then(result => result.upsertedId || id)

    }

    delete(collection, id) {
        return this.connect()
            .then(db => {
                return db.collection(collection).deleteOne({_id: ObjectId(id)});
            })
            .then(result => id)
    }
}

module.exports = MongoLib;
