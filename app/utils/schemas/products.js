const joi = require('@hapi/joi');

const productIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const productNameSchema = joi.string().max(80);
const productYearSchema = joi.number().min(1888);
const productPriceSchema = joi.number().min(0).max(99999);
const productCoverSchema = joi.string().uri();
const productCodeSchema = joi.string().min(3).max(255);

const createProductSchema = {
    name: productNameSchema.required(),
    price: productPriceSchema.required(),
    code: productCodeSchema
};

const updateProductSchema = {
    name: productNameSchema,
    price: productPriceSchema,
    code: productCodeSchema
};

module.exports = {
    productIdSchema,
    createProductSchema,
    updateProductSchema
};
