const productsMock = [{
  "id": 1,
  "name": "Cookies - Amaretto",
  "code": "0187-1616",
  "price": 54
}, {
  "id": 2,
  "name": "Crab - Dungeness, Whole",
  "code": "39822-4050",
  "price": 79
}, {
  "id": 3,
  "name": "Sausage - Chorizo",
  "code": "37205-488",
  "price": 49
}, {
  "id": 4,
  "name": "Muffin Mix - Blueberry",
  "code": "36987-1444",
  "price": 19
}, {
  "id": 5,
  "name": "Wine - Red, Cabernet Merlot",
  "code": "0603-1785",
  "price": 99
}, {
  "id": 6,
  "name": "Cheese Cheddar Processed",
  "code": "54868-4211",
  "price": 17
}, {
  "id": 7,
  "name": "Wine - Cotes Du Rhone Parallele",
  "code": "0406-0582",
  "price": 42
}, {
  "id": 8,
  "name": "Lidsoupcont Rp12dn",
  "code": "43269-761",
  "price": 67
}, {
  "id": 9,
  "name": "Liners - Banana, Paper",
  "code": "52380-7310",
  "price": 98
}, {
  "id": 10,
  "name": "Egg Patty Fried",
  "code": "37000-233",
  "price": 79
}, {
  "id": 11,
  "name": "Bread - Pumpernickel",
  "code": "24275-0185",
  "price": 93
}, {
  "id": 12,
  "name": "Cheese - Mozzarella, Shredded",
  "code": "52584-026",
  "price": 14
}, {
  "id": 13,
  "name": "Appetizer - Lobster Phyllo Roll",
  "code": "49999-316",
  "price": 4
}, {
  "id": 14,
  "name": "Juice - V8 Splash",
  "code": "53675-134",
  "price": 52
}, {
  "id": 15,
  "name": "Cookies Cereal Nut",
  "code": "63323-379",
  "price": 29
}, {
  "id": 16,
  "name": "Bread Foccacia Whole",
  "code": "63187-141",
  "price": 31
}, {
  "id": 17,
  "name": "Beer - Blue",
  "code": "49643-122",
  "price": 76
}, {
  "id": 18,
  "name": "Sardines",
  "code": "45945-156",
  "price": 81
}, {
  "id": 19,
  "name": "Shrimp - Black Tiger 26/30",
  "code": "36987-1306",
  "price": 15
}, {
  "id": 20,
  "name": "Dried Peach",
  "code": "68210-1901",
  "price": 89
}, {
  "id": 21,
  "name": "Bread - Italian Corn Meal Poly",
  "code": "25021-212",
  "price": 24
}, {
  "id": 22,
  "name": "Chip - Potato Dill Pickle",
  "code": "49288-0101",
  "price": 47
}, {
  "id": 23,
  "name": "Fiddlehead - Frozen",
  "code": "0641-0400",
  "price": 66
}, {
  "id": 24,
  "name": "Pepper - Red, Finger Hot",
  "code": "37000-103",
  "price": 53
}, {
  "id": 25,
  "name": "Chicken - Leg / Back Attach",
  "code": "0409-7938",
  "price": 10
}, {
  "id": 26,
  "name": "Sambuca - Ramazzotti",
  "code": "61734-301",
  "price": 42
}, {
  "id": 27,
  "name": "Veal - Ground",
  "code": "49738-101",
  "price": 88
}, {
  "id": 28,
  "name": "Pepper - Chillies, Crushed",
  "code": "52533-172",
  "price": 30
}, {
  "id": 29,
  "name": "Sugar Thermometer",
  "code": "76420-493",
  "price": 30
}, {
  "id": 30,
  "name": "Bread - Italian Corn Meal Poly",
  "code": "58118-0922",
  "price": 67
}, {
  "id": 31,
  "name": "Bandage - Finger Cots",
  "code": "0054-4570",
  "price": 100
}, {
  "id": 32,
  "name": "Bread - Flat Bread",
  "code": "50438-210",
  "price": 16
}, {
  "id": 33,
  "name": "Pork Salted Bellies",
  "code": "54575-353",
  "price": 41
}, {
  "id": 34,
  "name": "Cookie Dough - Chocolate Chip",
  "code": "10544-584",
  "price": 3
}, {
  "id": 35,
  "name": "Cherries - Maraschino,jar",
  "code": "50436-9036",
  "price": 71
}, {
  "id": 36,
  "name": "Veal - Tenderloin, Untrimmed",
  "code": "36987-3107",
  "price": 48
}, {
  "id": 37,
  "name": "Tomato - Plum With Basil",
  "code": "55714-2002",
  "price": 37
}, {
  "id": 38,
  "name": "Tomatoes - Cherry, Yellow",
  "code": "61715-090",
  "price": 55
}, {
  "id": 39,
  "name": "Broom - Push",
  "code": "59779-817",
  "price": 95
}, {
  "id": 40,
  "name": "Eggplant - Regular",
  "code": "62670-4369",
  "price": 21
}, {
  "id": 41,
  "name": "Savory",
  "code": "13734-143",
  "price": 20
}, {
  "id": 42,
  "name": "Beef - Bresaola",
  "code": "61958-0406",
  "price": 33
}, {
  "id": 43,
  "name": "Oil - Truffle, White",
  "code": "0113-0478",
  "price": 68
}, {
  "id": 44,
  "name": "Gatorade - Orange",
  "code": "54868-0767",
  "price": 69
}, {
  "id": 45,
  "name": "Flour - All Purpose",
  "code": "0093-0132",
  "price": 75
}, {
  "id": 46,
  "name": "Grenadillo",
  "code": "0173-0527",
  "price": 5
}, {
  "id": 47,
  "name": "Chilli Paste, Sambal Oelek",
  "code": "33261-796",
  "price": 16
}, {
  "id": 48,
  "name": "Pail With Metal Handle 16l White",
  "code": "41163-664",
  "price": 12
}, {
  "id": 49,
  "name": "Horseradish Root",
  "code": "49348-460",
  "price": 27
}, {
  "id": 50,
  "name": "Rice - Aborio",
  "code": "16477-185",
  "price": 44
}, {
  "id": 51,
  "name": "Eggroll",
  "code": "54575-135",
  "price": 30
}, {
  "id": 52,
  "name": "Trout - Rainbow, Frozen",
  "code": "65055-401",
  "price": 94
}, {
  "id": 53,
  "name": "Cheese - Ermite Bleu",
  "code": "58668-1409",
  "price": 35
}, {
  "id": 54,
  "name": "Cumin - Whole",
  "code": "0268-1284",
  "price": 9
}, {
  "id": 55,
  "name": "Ecolab Crystal Fusion",
  "code": "53603-4002",
  "price": 6
}, {
  "id": 56,
  "name": "Yogurt - French Vanilla",
  "code": "55154-7273",
  "price": 52
}, {
  "id": 57,
  "name": "Pasta - Canelloni, Single Serve",
  "code": "49288-0469",
  "price": 25
}, {
  "id": 58,
  "name": "Truffle Cups - White Paper",
  "code": "50730-8628",
  "price": 50
}, {
  "id": 59,
  "name": "Cranberries - Dry",
  "code": "63629-1679",
  "price": 46
}, {
  "id": 60,
  "name": "Vermouth - White, Cinzano",
  "code": "63629-3248",
  "price": 3
}, {
  "id": 61,
  "name": "Tarragon - Primerba, Paste",
  "code": "10237-644",
  "price": 74
}, {
  "id": 62,
  "name": "Cornish Hen",
  "code": "0268-1331",
  "price": 10
}, {
  "id": 63,
  "name": "Nantucket - Kiwi Berry Cktl.",
  "code": "52959-433",
  "price": 51
}, {
  "id": 64,
  "name": "Pie Box - Cello Window 2.5",
  "code": "68084-487",
  "price": 21
}, {
  "id": 65,
  "name": "Island Oasis - Mango Daiquiri",
  "code": "0615-1358",
  "price": 26
}, {
  "id": 66,
  "name": "Brandy - Orange, Mc Guiness",
  "code": "0268-6697",
  "price": 3
}, {
  "id": 67,
  "name": "Cheese - Wine",
  "code": "49288-0793",
  "price": 46
}, {
  "id": 68,
  "name": "Mountain Dew",
  "code": "61096-0023",
  "price": 99
}, {
  "id": 69,
  "name": "Pork Ham Prager",
  "code": "55264-021",
  "price": 42
}, {
  "id": 70,
  "name": "Cookie Choc",
  "code": "35356-055",
  "price": 93
}, {
  "id": 71,
  "name": "Wine - Prosecco Valdobienne",
  "code": "11673-063",
  "price": 5
}, {
  "id": 72,
  "name": "Tobasco Sauce",
  "code": "65044-3626",
  "price": 57
}, {
  "id": 73,
  "name": "Wine - Harrow Estates, Vidal",
  "code": "67777-234",
  "price": 15
}, {
  "id": 74,
  "name": "Juice - V8 Splash",
  "code": "63736-375",
  "price": 48
}, {
  "id": 75,
  "name": "Chicken Breast Wing On",
  "code": "0187-5170",
  "price": 11
}, {
  "id": 76,
  "name": "Pork - Bacon Cooked Slcd",
  "code": "0615-7566",
  "price": 42
}, {
  "id": 77,
  "name": "Scallops 60/80 Iqf",
  "code": "60429-214",
  "price": 7
}, {
  "id": 78,
  "name": "Bag - Clear 7 Lb",
  "code": "0006-0464",
  "price": 32
}, {
  "id": 79,
  "name": "Cornstarch",
  "code": "67877-167",
  "price": 79
}, {
  "id": 80,
  "name": "Vermacelli - Sprinkles, Assorted",
  "code": "54868-4767",
  "price": 44
}, {
  "id": 81,
  "name": "Potatoes - Parissienne",
  "code": "41163-251",
  "price": 64
}, {
  "id": 82,
  "name": "Pastry - Key Limepoppy Seed Tea",
  "code": "51346-015",
  "price": 70
}, {
  "id": 83,
  "name": "Wine - Jackson Triggs Okonagan",
  "code": "0270-1316",
  "price": 4
}, {
  "id": 84,
  "name": "Veal - Inside, Choice",
  "code": "0682-1570",
  "price": 57
}, {
  "id": 85,
  "name": "Lotus Leaves",
  "code": "46123-031",
  "price": 71
}, {
  "id": 86,
  "name": "Nantuket Peach Orange",
  "code": "0113-0294",
  "price": 33
}, {
  "id": 87,
  "name": "Coke - Classic, 355 Ml",
  "code": "68356-109",
  "price": 56
}, {
  "id": 88,
  "name": "Fish - Scallops, Cold Smoked",
  "code": "49288-0645",
  "price": 37
}, {
  "id": 89,
  "name": "Island Oasis - Peach Daiquiri",
  "code": "0703-4155",
  "price": 53
}, {
  "id": 90,
  "name": "Oil - Grapeseed Oil",
  "code": "51760-001",
  "price": 6
}, {
  "id": 91,
  "name": "Island Oasis - Strawberry",
  "code": "0363-0047",
  "price": 97
}, {
  "id": 92,
  "name": "Tray - 12in Rnd Blk",
  "code": "13733-010",
  "price": 12
}, {
  "id": 93,
  "name": "Island Oasis - Mango Daiquiri",
  "code": "52915-020",
  "price": 79
}, {
  "id": 94,
  "name": "Aromat Spice / Seasoning",
  "code": "47682-488",
  "price": 70
}, {
  "id": 95,
  "name": "Pate Pans Yellow",
  "code": "61924-405",
  "price": 94
}, {
  "id": 96,
  "name": "Red Currants",
  "code": "43353-492",
  "price": 21
}, {
  "id": 97,
  "name": "Bread - English Muffin",
  "code": "51672-4022",
  "price": 59
}, {
  "id": 98,
  "name": "V8 - Tropical Blend",
  "code": "43419-750",
  "price": 41
}, {
  "id": 99,
  "name": "Beans - Butter Lrg Lima",
  "code": "53798-010",
  "price": 65
}, {
  "id": 100,
  "name": "Egg - Salad Premix",
  "code": "49288-0696",
  "price": 82
}];

function filteredProductsMock(code) {
    return productsMock.filter(product => product.code.includes(code));
}

function findProduct(id) {
    return productsMock.find((product) => product.id == id);
}

class ProductsServiceMock {
    

    async getProducts() {
        return Promise.resolve(productsMock);
    }

    async getProduct(productId) {
        //let product = null;
        //for (let i = 0; i < productsMock.length ; i++) {
        //    if (productsMock[i].id == productId.productId) {
        //        product = productsMock[i];
        //        break;
        //    }
        //}
        //return Promise.resolve(productsMock[productId]);
        //return Promise.resolve(product);
        return Promise.resolve(findProduct(parseInt(productId)));

    }

    async createProduct() {
        return Promise.resolve(productsMock[0]);
   }
}


module.exports = {
    productsMock,
    filteredProductsMock,
    findProduct,
    ProductsServiceMock
};
